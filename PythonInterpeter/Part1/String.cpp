#include "String.h"


String::String(std::string s) : Sequence()
{
	this->_str = s;
}

std::string String::toString() const
{
	return _str;
}