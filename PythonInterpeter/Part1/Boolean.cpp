#include "Boolean.h"

//C'tor
Boolean::Boolean(bool value) : Type(false)
{
	this->_value = value;
}

/*This function reutns the value as stirng
Out: Value as string type
*/
std::string Boolean::toString() const
{
	return (_value ? "1" : "0");
}