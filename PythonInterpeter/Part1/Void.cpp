#include "Void.h"

//C'Tor
Void::Void(void* value) : Type(false)
{
	this->_value = value;
}


//override function isPrintable
bool Void::isPrintable()
{
	return false;
}