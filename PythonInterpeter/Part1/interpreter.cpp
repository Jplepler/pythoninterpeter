#include "type.h"
#include "IndentationException.h"
#include "SyntaxException.h"
#include "parser.h"
#include <iostream>

#define WELCOME "Welcome to Magshimim Python Interperter version 1.0 by "
#define YOUR_NAME "Jonathan Plepler"

using namespace std;

int main(int argc,char **argv)
{
	std::cout << WELCOME << YOUR_NAME << std::endl;

	std::string input_string;
	Type* theType;
	// get new command from user
	std::cout << ">>> ";
	std::getline(std::cin, input_string);
	
	while (input_string != "quit()")
	{
		// prasing command
		try {
			theType = Parser::parseString(input_string);
			if (theType->isPrintable())
			{
				cout << theType->toString() << endl;
			}
		}
		//Catch the exceptions
		catch (IndentationException & e)
		{
			cout << e.what() << endl;
		}
		catch (SyntaxException& e)
		{
			cout << e.what() << endl;
		}

		// get new command from user
		std::cout << ">>> ";
		std::getline(std::cin, input_string);
	}

	return 0;
}


