#include "type.h"

//C'Tor
Type::Type(bool isTemp)
{
	this->_isTemp = isTemp;
}


//Setter
void Type::setIsTemp(bool value)
{
	this->_isTemp = value;
}

//Getter
bool Type::getIsTemp()
{
	return this->_isTemp;
}

bool Type::isPrintable() const
{
	return true;
}

