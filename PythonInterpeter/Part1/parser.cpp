#include "parser.h"


/*This function removes whitespaces for end and checks if the type is valid (if not throws exception)
In: The users input
Out: the type of the input
*/
Type* Parser::parseString(std::string str) throw()
{
	Type* theType;
	Helper h;
	h.rtrim(str);//Remove whitespaces after content
	if (str.length() > 0 && str[0] != ' ' && str[0] != '\t')
	{
		theType = getType(str);//Get the type
		if (theType == NULL)
		{
			throw SyntaxException();
		}
	}
	else
	{
		throw IndentationException();
	}
	

	return theType;
}

/*Checks of what type is the string
In: refrence to the string that the user inputted
Out: type of string
*/
Type* Parser::getType(std::string& str)
{
	Type* theType;
	Helper h;
	//Check if integer
	if (h.isInteger(str))
	{
		try {
			theType = new Integer(stoi(str));
			return theType;
		}
		catch (...)
		{
			//Will print later syntax error
		}
		
	}

	//Check if stirng
	if (h.isString(str))
	{
		theType = new String(str);
		return theType;
	}

	//Check if bool
	if (h.isBoolean(str))
	{
		theType = new Boolean((str == "True" ? true : false));
		return theType;
	}

	return NULL;
}

bool Parser::isLegalVarName(const std::string& str)
{
	Helper h;
	int i = 0;
	if (h.isDigit(str[0]) && (h.isLetter(str[0] || h.isUnderscore(str[0]))))
	{
		return false;
	}
	for(i = 1; i < str.length(); i++)
	{
		if (!(h.isLetter(str[0] || h.isUnderscore(str[0]) || h.isDigit(str[0]))))
		{
			return false;
		}
	}

	return true;
}


