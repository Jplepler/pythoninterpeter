#ifndef VOID_H
#define VOID_H
#include "type.h"
class Void : public Type
{
public:
	Void(void* value);
	bool isPrintable();


private:
	void* _value;

};

#endif // VOID_H