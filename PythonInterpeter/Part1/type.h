#ifndef TYPE_H
#define TYPE_H
#include <iostream>
#include <string>
class Type
{
public:
	Type(bool isTemp);
	void setIsTemp(bool value);
	bool getIsTemp();
	virtual bool isPrintable() const;
	virtual std::string toString() const = 0;

protected:
	bool _isTemp;


};





#endif //TYPE_H
