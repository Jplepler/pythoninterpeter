#include "Integer.h"

//C'Tor
Integer::Integer(int value) : Type(false)
{
	this->_value = value;
}

/*This function reutns the value as stirng
Out: Value as string type
*/
std::string Integer::toString() const
{
	return std::to_string(_value);
}